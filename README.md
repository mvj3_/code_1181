实现循环ScrollView。有以下特色：

    1、循环的scrollview

    2、类似于tableview的编程方式

    3、可定制化的内容

    4、灵活运用可用于异步加载图片

    5、结构化，可扩展性高
